object Dependencies {
  val cats = "1.0.1"
  val fs2 = "0.10.0"
  val scalatest = "3.0.0"
  val scalacheck = "1.13.5"
  val http4s = "0.18.0"
  val circe = "0.9.1"
  val shapeless = "2.3.3"
  val scodecCore = "1.10.3"
}
